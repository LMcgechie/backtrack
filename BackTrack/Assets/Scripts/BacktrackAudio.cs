﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BacktrackAudio : MonoBehaviour
{
    [SerializeField] GameObject[] backtrackedAudio;
    bool backtracked = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && !backtracked)
        {
            foreach (GameObject audio in backtrackedAudio)
            {
                try
                {
                    audio.SetActive(true);
                }
                catch (NullReferenceException ex)
                {
                    Debug.Log("probably no audio triggers in level");
                }
            }
            backtracked = !backtracked;
        }
    }
}
