﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class CurrentPlayer : MonoBehaviour
{
    [SerializeField] List<GameObject> players = new List<GameObject>();
    [SerializeField] List<GameObject> cameras = new List<GameObject>();
    [SerializeField] List<GameObject> model = new List<GameObject>();
    GameObject player;
    GameObject currentCamera;
    PlayerBacktrack SB;
    Vector3 player0StartPos, player1StartPos;
    enum state {start,P1 ,P2};
    [SerializeField]state currentstate = state.start;
    // Start is called before the first frame update
    void Start()
    {
        
        player = players[0];
        SB = player.GetComponent<PlayerBacktrack>();
        SB.setActive();
        SB.StartBacktrack();
        player0StartPos = players[0].GetComponent<Transform>().transform.position;

        //Enable P1 to collect starting position then disable to hide P1
        players[1].SetActive(true);
        player1StartPos = players[1].GetComponent<Transform>().transform.position;
        players[1].GetComponent<PlayerBacktrack>().setStartPos(player1StartPos);
        players[1].SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        if (currentstate == state.start && Input.GetKeyDown(KeyCode.E))
        {
            ResetPosition();
            ActivateP2();
            DisableP1();
            StartP1BackTrack();
            currentstate = state.P2;
        }
        else if (currentstate == state.P1 && Input.GetKeyDown(KeyCode.E))
        {
            ResetPosition();
            EnableP2();
            DisableP1();
            StartP1BackTrack();
            currentstate = state.P2;
        }
        else if (currentstate == state.P2 && Input.GetKeyDown(KeyCode.E))
        {
            ResetPosition();
            EnableP1();
            DisableP2();
            StartP2BackTrack();
            currentstate = state.P1;
        }


    }

    private void ResetPosition()
    {
        players[0].transform.position = player0StartPos;
        players[1].transform.position = player1StartPos;
    }

    private void StartP2BackTrack()
    {
        player = players[1];
        player.GetComponent<PlayerBacktrack>().StartBacktrack();
    }

    private void StartP1BackTrack()
    {
        player = players[0];
        player.GetComponent<PlayerBacktrack>().StartBacktrack();
    }

    private void EnableP2()
    {
        player = players[1];
        currentCamera = cameras[1];
        currentCamera.SetActive(true);
        player.GetComponent<PlayerController>().enabled = true;
        player.GetComponent<PlayerBacktrack>().setActive();
        model[1].SetActive(false);
    }

    private void EnableP1()
    {
        player = players[0];
        currentCamera = cameras[0];
        player.GetComponent<PlayerController>().enabled = true;
        player.GetComponent<PlayerBacktrack>().setActive();
        currentCamera.SetActive(true);
        model[0].SetActive(false);
    }

    private void DisableP2()
    {
        player = players[1];
        currentCamera = cameras[1];
        player.GetComponent<PlayerController>().enabled = false;
        player.GetComponent<PlayerBacktrack>().setActive();
        currentCamera.SetActive(false);
        model[1].SetActive(true);
    }

    private void DisableP1()
    {
        player = players[0];
        currentCamera = cameras[0];
        player.GetComponent<PlayerController>().enabled = false;
        player.GetComponent<PlayerBacktrack>().setActive();
        currentCamera.SetActive(false);
        model[0].SetActive(true);
    }

    private void ActivateP2()
    {
        player = players[1];
        player.SetActive(true);
        player.GetComponent<PlayerBacktrack>().setActive();
    }
}
