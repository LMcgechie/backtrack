﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] bool left;
    [SerializeField] PressurePlate[] pps;
    Rigidbody doorBody;
    bool moved = false;
    float distance = 3f;
    Vector3 closePos;
    Vector3 openPos;

    void Start()
    {
        doorBody = GetComponent<Rigidbody>();
        closePos = GetComponent<Transform>().position;
        if (left)
            openPos = closePos + new Vector3(0,0, distance);
        else
            openPos = closePos + new Vector3(0, 0, -distance);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        if (ppchecker())
        {
            transform.position = Vector3.Lerp(transform.position, openPos, 0.125f);
        }

        if (!ppchecker())
        {
            transform.position = Vector3.Lerp(transform.position, closePos, 0.125f);
        }
    }

    private bool ppchecker()
    {
        foreach (PressurePlate pp in pps)
        {
            if (!pp.GetActive())
                return false;
        }
        return true;
    }


}
