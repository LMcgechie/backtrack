﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class voiceTrigger : MonoBehaviour
{
    [SerializeField] VoiceLines voice;
    [SerializeField] int triggerNum;
    bool triggered = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("Player"))
        {
            triggered = true;
            voice.PlayClip(triggerNum);
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name.Contains("Player"))
            gameObject.SetActive(false);
    }

 
}
