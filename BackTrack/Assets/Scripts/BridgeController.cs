﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeController : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] float distance;
    [SerializeField] PressurePlate[] pps;
    Vector3 closePos;
    Vector3 openPos;

    void Start()
    {
        closePos = GetComponent<Transform>().position;
        openPos = closePos + new Vector3(distance, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void FixedUpdate()
    {
        if (ppchecker())
        {
            transform.position = Vector3.Lerp(transform.position, openPos, 0.125f);
        }

        if (!ppchecker())
        {
            transform.position = Vector3.Lerp(transform.position, closePos, 0.125f);
        }
    }

    private bool ppchecker()
    {
        foreach (PressurePlate pp in pps)
        {
            if (!pp.GetActive())
                return false;
        }
        return true;
    }
}
