﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class PlayerAnimControl : MonoBehaviour
{
    Animator myAnims;
    Vector3 previousPos;
    Vector3 currentPos;
    bool forward = false;
    bool left = false;
    bool right = false;
    bool moving = false;


    [SerializeField] AudioSource sound;
    
    // Start is called before the first frame update
    void Start()
    {
        myAnims = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        currentPos = transform.position;


        if (currentPos.x > previousPos.x && (Math.Abs(currentPos.x - previousPos.x) > Math.Abs(currentPos.z - previousPos.z)))
        {            
            forward = true;
            right = false;
            left = false;
        }
        else if (currentPos.z > previousPos.z && (Math.Abs(currentPos.z - previousPos.z) > Math.Abs(currentPos.x - previousPos.x)))
        {
            forward = false;
            right = true;
            left = false;
        }
        else if (currentPos.z < previousPos.z && (Math.Abs(currentPos.z - previousPos.z) > Math.Abs(currentPos.x - previousPos.x)))
        {
            forward = false;
            right = false;
            left = true;
        }
        else if (currentPos == previousPos)
        {            
            forward = false;
            right = false;
            left = false;
        }

        PlayAnim();
        previousPos = currentPos;
    }

    void PlayAnim()
    {
        myAnims.SetBool("forward", forward);
        myAnims.SetBool("left", left);
        myAnims.SetBool("right", right);
    }
}
