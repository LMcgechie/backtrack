﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    
    [SerializeField] CharacterController controller;
    float speed = 6f;
    float gravity = -29.43f;
    float jumpHeight = 1.2f;
    float pushPower = 2f;

    [SerializeField] Transform groundCheck;
    [SerializeField] LayerMask groundMask;
    [SerializeField] LayerMask cubeMask;
    public float groundDistance = 0.161f;    
    Vector3 velocity;
    bool isGrounded;
    Rigidbody body;
    bool walking;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        //Ground check for applying gravity
        isGrounded = Physics.Raycast(groundCheck.position, transform.TransformDirection(Vector3.down), groundDistance, groundMask);

        //If player model is on the ground gravity is set to -3
        //Due to raycast meeting the ground before the player does the -1 guarantees the player model gets to the gound
        if (isGrounded && velocity.y < 0f) 
        {
            velocity.y = -3f;
        }

        //Input for play movement
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        //Translate input to player based on where the camera is facing
        Vector3 move = transform.right * x + transform.forward * z;
        //Moves player relative to time past to avoid player moving faster or slower due to frame rate
        //controller.Move(move * speed * Time.deltaTime);
        //Vector3 alpha = (move * speed) + velocity;

        //Calculate force for jump
        if (Input.GetButtonDown("Jump") && isGrounded) 
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }

        //Applying Gravity
        velocity.y += gravity * Time.deltaTime;
        Vector3 alpha = (move * speed) + velocity;
        controller.Move(alpha * Time.deltaTime);
    }




    public bool GetGrounded() {
        return isGrounded;
    }
}
