﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ObjectBackTrack : MonoBehaviour
{
    
    protected bool isBacktracking = false;

    protected Vector3 startPos;

    protected List<Points> points;

    // Start is called before the first frame update
    protected void Start()
    {
        points = new List<Points>();
        startPos = gameObject.transform.position;
    }

    // Update is called once per frame
    protected void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && isBacktracking)
        {
            transform.position = startPos;
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            StartBacktrack();
        }
    }

    protected void FixedUpdate()
    {
        if (isBacktracking)
            backtrack();
        else
            Record();
    }

    protected void backtrack()
    {
        
        if (points.Count > 0)
        {
            Points point = points[0];
            transform.position = point.position;
            transform.rotation = point.rotation;
            points.RemoveAt(0);
        }
        else
        {
            StopBacktrack();
        }
        
    }

    protected void Record()
    {
        
        points.Add(new Points(transform.position, transform.rotation));
    }

    public void StartBacktrack()
    {
        isBacktracking = true;
    }

    protected void StopBacktrack()
    {
        isBacktracking = false;
    }

    public bool GetBacktracking()
    {
        return isBacktracking;
    }

    public void setStartPos(Vector3 newStartPos)
    {
        startPos = newStartPos;
    }

    public int getPoints()
    {
        return points.Count;
    }
}
