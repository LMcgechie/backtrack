﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour
{
    private bool active = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {        
        if (other.gameObject.name.Contains("Cube") || other.gameObject.name.Contains("Player"))
            active = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name.Contains("Cube") || other.gameObject.name.Contains("Player"))
            active = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name.Contains("Cube") || other.gameObject.name.Contains("Player"))
            active = false;
    }

    public bool GetActive()
    {
        return active;
    }
}
