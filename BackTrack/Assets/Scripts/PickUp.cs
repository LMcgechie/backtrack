﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Transactions;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public float pickUpDistance = 5f;
    bool holdingSomething = false;
    RaycastHit hit;
    Ray ray;
    [SerializeField] LayerMask cubeMask;
    [SerializeField] GameObject carryPoint;
    GameObject cube;
    GameObject point;


    private void Update()
    {
        ray = new Ray(carryPoint.transform.position, Vector3.down);
        Physics.Raycast(carryPoint.transform.position, transform.TransformDirection(Vector3.down), out hit, pickUpDistance, cubeMask);
        Debug.DrawRay(carryPoint.transform.position, Vector3.down, Color.green);
        try
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                PickUpObject(hit);
            }
        }
        catch (NullReferenceException ex)
        { 
        }
        try
        {
            if (Input.GetKeyDown(KeyCode.E) && holdingSomething)
                PickUpObject(hit);
        }
        catch(NullReferenceException ex) { }
    }

    void PickUpObject(RaycastHit newHit)
    {
        try
        {

            if (!holdingSomething)
            {
                newHit.collider.GetComponent<Rigidbody>().useGravity = false;
                newHit.collider.transform.position = carryPoint.transform.position - new Vector3(-0.5f, 1f, 0);
                newHit.collider.transform.parent = carryPoint.transform;
                holdingSomething = !holdingSomething;
                cube = newHit.collider.gameObject;
            }
            else if (holdingSomething)
            {
                cube.transform.parent = null;
                cube.GetComponent<Rigidbody>().useGravity = true;
                holdingSomething = !holdingSomething;
            }
        }
        catch (NullReferenceException ex)
        {
            Debug.Log("probably not close enough to box");
        }
    }

    public bool getHolding()
    {
        return holdingSomething;
    }
}