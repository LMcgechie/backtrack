﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewBehaviourScript : MonoBehaviour
{

    public void LoadLevel1()
    {
        SceneManager.LoadScene("Level1");
    }

    public void QuitGame() 
    {
        Application.Quit();
    }
}
