﻿
using UnityEngine;

public class Points
{
    public Vector3 position;
    public Quaternion rotation;

    public Points(Vector3 _position, Quaternion _rotation)
    {
        position = _position;
        rotation = _rotation;
    }
}
