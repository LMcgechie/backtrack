﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBacktrack : ObjectBackTrack
{
    private bool active = false;


    private new void FixedUpdate()
    {
        

        if (GetBacktracking())
        {
            
            backtrack();
        }
        if (!GetBacktracking() && active)
        {
            Record();
        }
    }

    public void setActive()
    {
        active = !active;
        if (active)
        {
            try
            {
                transform.position = startPos;
                base.points.Clear();
            }
            catch (NullReferenceException ex)
            {
                Debug.Log("Probably in start state");
            }

        }
    }
}
